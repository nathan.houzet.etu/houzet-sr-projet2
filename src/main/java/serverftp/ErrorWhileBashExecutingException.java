package serverftp;

/***
 * @author Nathan Houzet
 * Exception for the errors while executing bash commands
 */
public class ErrorWhileBashExecutingException extends Exception {
    /***
     * personalised exception
     * @param errorMessage the error message to be display if the exception is raise
     */
    public ErrorWhileBashExecutingException(String errorMessage) {
        super(errorMessage);
    }
}