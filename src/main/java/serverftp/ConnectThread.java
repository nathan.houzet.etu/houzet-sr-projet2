package serverftp;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/***
 * @author Nathan Houzet
 * This class is used to represent a client who is connected to the server
 */
public class ConnectThread extends Thread {
    Server server;
    Socket s;
    InputStreamReader isr;
    BufferedReader reader;
    PrintWriter sendCommand;
    boolean logIn;
    boolean runnable;
    String type;
    int passivePort;
    ServerSocket pss;
    private Socket passiveSocket;

    /***
     * Constructor of the ConnectedThread, initialize the client like that output, and input stream.
     * @param new_connection the socket returned by the accept method
     * @throws IOException if they are a problem while creating the streams
     */
    public ConnectThread(Socket new_connection) throws IOException {
        this.s = new_connection;
        this.isr = new InputStreamReader(s.getInputStream());
        this.reader = new BufferedReader(this.isr);
        this.sendCommand = new PrintWriter(s.getOutputStream(), true);
        this.logIn=false;
        this.runnable = true;


    }

    /***
     * used to connect the ftp server with the thread representing one client
     * @param server the ftp server
     */
    public void setServer(Server server) {
        this.server = server;
    }

    /***
     * Used to receive the last command sent from the client
     * @return the last command of the client
     * @throws ErrorOfSystemReadingException if they are a problem while reading the command
     */
    public synchronized String  receiveProcedure() throws ErrorOfSystemReadingException {
        try {

            return reader.readLine();

        } catch (IOException e) {
            throw new ErrorOfSystemReadingException("Erreur lors de la lecture du Input stream");
        }
    }

    /***
     * Sent the welcome message to the client
     */
    public void welcome() {
        sendCommand.println("220 Service ready \r");
    }

    /***
     * Send the confirmation to the client that his username is ok and he can sent his password now.
     */
    public void userOK() {
        sendCommand.println("31 User name ok, need password\r");
    }

    /***
     * Check if a user if well known by the ftp server
     * @param user the username of the user
     * @return true if the user if known, false else
     */
    public boolean checkUser(String user) {
        return this.server.users.containsKey(user);
    }

    /***
     * Check if the user and password of the client are matching.
     * @param user the id of the user
     * @param pass the password of the user
     * @return true if the client is known by the ftp server, else false.
     */
    public boolean checkPassword(String user, String pass) {
        return this.server.users.get(user).equals(pass);
    }

    /***
     * Send a message to the client that he is well logged in.
     */
    private void loggedInMessage() {
        sendCommand.println("230 User logged in\r");

    }

    /***
     * Method that allow to authentifiate the client in the ftp server.
     * @return true if the client is well logged in, false else
     * @throws ErrorOfSystemReadingException if a connection command fails
     */
    public synchronized boolean authentication() throws ErrorOfSystemReadingException {
        welcome();
        String res = receiveProcedure();
        while(!(res.startsWith("USER"))){

            sendCommand.println("553 Requested action not taken.\r");
            res = receiveProcedure();
        }
        if (checkUser(res.substring(5))) {
            String user = res.substring(5);
            userOK();
            res = receiveProcedure();
            if (checkPassword(user, res.substring(5))) {
                loggedInMessage();
                return true;
            }else{
                sendCommand.println("530 Not logged in\r");
                runnable=false;
                return false;
            }
        } else {
            sendCommand.println("530 Not logged in\r");
            runnable=false;
            return false;

        }
    }

    /***
     * Each thread will follow this method once created.
     * they will first try to connect to the client, and then listen to their commands and call the adequate methods
     */
    public void run() {
        try {
            boolean knowCommand;
            while (runnable) {
                if (logIn) {
                    knowCommand = false;
                    String res = receiveProcedure();
                    if(res==(null)||res.length()==0){
                        sendCommand.println("500 Syntax error, command unrecognized.\r");
                    }
                    System.out.println(res);
                    if (res != null && res.startsWith("PWD")) {
                        printWorkingDirectory();
                        knowCommand = true;
                    }
                    if (res != null && res.startsWith("TYPE")) {
                        typeRemember(res.substring(5));
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("PASV")) {
                        SetPassiveMode();
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("LIST")) {
                        ListCommand(res.substring(4));
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("MKD")) {
                        mkdir(res.substring(4));
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("SYST")){
                        syst();
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("CWD")){
                        if (res.substring(4).equals("/")){
                            cdUp();
                        }else{
                            cd(res.substring(4));
                        }

                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("CDUP")){
                        cdUp();
                        knowCommand=true;
                    }
                    if (res != null && res.startsWith("RMD")){
                        remove(res.substring(4));
                        knowCommand=true;
                    }
                    if(! knowCommand) {
                        //si la commande n'est pas implementer, on signale que le serveur ftp ne supporte pas cette commande
                        sendCommand.println("202 Command not implemented, superfluous at this site.");
                    }
                } else {
                    logIn = authentication();
                }
            }
            this.interrupt();

        } catch (ErrorOfSystemReadingException | ErrorOfInitServerSocketException | IOException | ErrorOfInitStreamException | ErrorWhileBashExecutingException e) {
            e.printStackTrace();
        }
    }

    /***
     * Method used to implement the RMD FTP command
     * @param name the name of the directory to remove
     */
    private void remove(String name) {
        File file =new File("this.server.currentRepertory/"+name);
        if (file.delete()) {
            sendCommand.println("250 Requested file action okay, completed.\r");
        } else {
            sendCommand.println("550 Requested action not taken.\r");
        }
    }

    /***
     * method that implement the FTP cd commmand
     * @param name the name of the file to move in
     */
    private void cd(String name) {
        if (name.startsWith(this.server.root)){
            this.server.currentRepertory = name;
            sendCommand.println("200 Command OK\r");
        }else{
            sendCommand.println("550 Failed to change directory.\r");
        }

    }

    /***
     * Method used to implement the cd Up ftp command.
     */
    private void cdUp() {
        if (server.currentRepertory.equals(server.root)){
            sendCommand.println("550 Failed to change directory.\r");
        }else{
            String[] rep = this.server.currentRepertory.split("/");
            String res="";
            for(int i=1;i<rep.length-1;i++){
                res+="/"+rep[i];
            }
            this.server.currentRepertory=res;
            sendCommand.println("200 Command OK\r");
        }
    }

    /***
     * Sent the answer to the SYST FTp command
     * it sent the type of machine where the server is running
     */
    private void syst() {
        sendCommand.println("215 UNIX Type:L8\r");
    }

    /***
     * Function that create a repertory on the ftp server
     * @param name the name of the new repertory.
     */
    private void mkdir(String name) {
        File toCreate=new File(server.serverDir.toString()+"/"+name);
        if (toCreate.mkdir()) {
            sendCommand.println("257 \"" + name + "\" created.\r");
        }else{
            sendCommand.println("451 Requested action aborted: local error in processing.\r");
        }
    }

    /***
     * Fonction used to set up a passive mode between the client and the ftp server.
     * @throws ErrorOfInitServerSocketException if the server socket for the data have a problem of creation
     */
    private void SetPassiveMode() throws ErrorOfInitServerSocketException {
        this.passivePort = (int) (100 + (Math.random() * (200 - 100)));
        sendCommand.println("200 Entering Passive Mode (127,0,0,1,"+10+","+passivePort+")\r");
        try{
            this.pss = new ServerSocket((10*256)+passivePort);
            this.passiveSocket=pss.accept();
        } catch (IOException e) {
            sendCommand.println("425 Can’t open data connection.\r");
            throw new ErrorOfInitServerSocketException("Erreur lors de la création du server socket de data lors de la mise en mode passive");

        }

    }


    /***
     * Method that send the current working directory to the client
     */
    private void printWorkingDirectory() throws ErrorOfSystemReadingException {
        sendCommand.println("200 \""+this.server.currentRepertory+"\" is the current directory.\r");
    }

    /**
     * Function used to remember the type of ftp transfer asked by the client
     * ASCII or Binary are available
     * @param type the type of transfer
     */
    private void typeRemember(String type) {
        this.type = type;
        sendCommand.println("200 OK");
    }

    /***
     * Method used to execute the list command
     * @param file the name of the file that have to be list, if this String is empty, the current repertory have to be list
     * @throws ErrorOfInitStreamException if the passive socket can't be created
     * @throws IOException if the output stream can't be initialize
     */
    private void ListCommand(String file) throws ErrorOfInitStreamException, ErrorWhileBashExecutingException, IOException {
        if (this.type.equals("I")){
            try{
                OutputStream dataSend = passiveSocket.getOutputStream();
                file=file.trim();
                if (file.equals("")){
                    String s;
                    Process p;

                    try {
                        p = Runtime.getRuntime().exec("ls -al /tmp/server");
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(p.getInputStream()));
                        while ((s = br.readLine()) != null) {
                            sendResponse(dataSend, s);
                        }
                        p.waitFor();
                        p.destroy();
                    } catch (Exception e) { throw new ErrorWhileBashExecutingException("Erreur lors de l'execution du bash dans LIST");}
                }
                sendCommand.println("226 Closing data connection.\n");
                passiveSocket.close();

            }catch (IOException e){throw new ErrorOfInitStreamException("Erreur lors de initialisation du stream sur la socket passive"); }
        }
    }

    /***
     * Methof used to send String in binary to an output Stream
     * @param datasend the output stream where to send the String
     * @param string the string to be sent
     * @throws IOException if the binary sending have a problem
     */
    private void sendResponse(OutputStream datasend, String string) throws IOException {
        datasend.write(string.getBytes());
        datasend.flush();
    }


}


