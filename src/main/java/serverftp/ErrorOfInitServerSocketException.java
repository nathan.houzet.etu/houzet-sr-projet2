package serverftp;

/***
 * @author Nathan Houzet
 * Exception for errors while creating a server Socket
 */
public class ErrorOfInitServerSocketException extends Exception {
    /***
     * personalised exception
     * @param errorMessage the error message to be display if the exception is raise
     */
    public ErrorOfInitServerSocketException(String errorMessage) {
        super(errorMessage);
    }
}