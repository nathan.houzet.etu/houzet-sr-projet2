package serverftp;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;

/***
 * @author Nathan Houzet
 * This class is used to initialise the ftp server.
 * It contain also a lot of parameters such the current directory of the server, or the hash map containing the couples users, passwords
 */
public class Server {
    ServerSocket ss;
    HashMap<String,String> users;
    File serverDir;
    String currentRepertory;
    String root;

    /***
     * Constructor, mainly create the serversocket
     * @throws ErrorOfInitServerSocketException if they are a problem while creating the server socket.
     * @param port the port of the FTP server
     */
    public Server(int port) throws ErrorOfInitServerSocketException {
        try{this.ss = new ServerSocket(port);
    } catch (IOException e) {
        throw new ErrorOfInitServerSocketException("Erreur lors de la création du server socket de control");
    }
        users = new HashMap<String,String>();
    }

    /***
     * @return the serverSocket
     */
    public ServerSocket getServerSocket(){
        return this.ss;
    }

    /***
     * Function that allow to add an user in the ftp server
     * @param name the id of the new user
     * @param password the password of the new user
     */
    public void addUser(String name, String password){
        users.put(name,password);
    }

    /***
     * Used to init the ftp local repertory
     * @param repository the name of the repertory
     * @return true if the repertory was created, false if it was already exist.
     */
    public Boolean initRepository(String repository){
        this.currentRepertory=repository;
        this.root=repository;
        serverDir = new File(repository);
        if (serverDir.exists()){
            return false;
        }else{
            return serverDir.mkdir();
        }


    }


}
