package serverftp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Test for the ConnectedThread class.
 */
public class ConnectedThreadTest {
    Server server;
    ConnectThread thd;
    Socket client;
    PrintWriter clientSent;
    BufferedReader clientReceptor;

    @Before
    public void initServer() throws ErrorOfInitServerSocketException, IOException {
        int port = (int) (8000 + (Math.random() * (9000 - 8000)));
        this.server =  new Server(port);
        this.client = new Socket("127.0.0.1",port);
        this.thd = new ConnectThread(server.ss.accept());
        this.clientSent = new PrintWriter(client.getOutputStream(), true);
        this.clientReceptor = new BufferedReader(new InputStreamReader(client.getInputStream()));


    }

    @Test
    public void testSetServer(){
        this.thd.setServer(this.server);
        assertEquals(thd.server,this.server);
    }

    @Test
    public void testReceiveProcedure() throws ErrorOfSystemReadingException {
        this.clientSent.println("200 Command Ok.\r");
        assertEquals(this.thd.receiveProcedure(), ("200 Command Ok."));
    }

    @Test
    public void TestWelcome() throws IOException {
        thd.welcome();
        assertEquals("220 Service ready ",this.clientReceptor.readLine());
    }

    @Test
    public void TestUserOK() throws IOException {
        thd.userOK();
        assertEquals("31 User name ok, need password",this.clientReceptor.readLine());
    }

    @Test
    public void TestCheckUserFalse() {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        assertFalse(thd.checkUser("toto"));
    }

    @Test
    public void TestCheckPasswordTrue(){
        thd.setServer(this.server);
        server.addUser("admin","admin");
        assertTrue(thd.checkPassword("admin","admin"));
    }

    @Test
    public void TestCheckPasswordFalse(){
        thd.setServer(this.server);
        server.addUser("admin","admin");
        assertNull(server.users.get("toto"));
    }
    @Test
    public void TestLoggedInMessage() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        thd.start();

        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        assertEquals("31 User name ok, need password",this.clientReceptor.readLine());
        thd.runnable=false;
    }

    @Test
    public void TestNotLoggedInMessage() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER toto");
        assertEquals("530 Not logged in",this.clientReceptor.readLine());
        //thd.runnable=false;
    }

    @Test
    public void TestSystCommand() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("SYST\r\n");
        assertEquals("215 UNIX Type:L8",this.clientReceptor.readLine());
        thd.runnable=false;
    }

    @Test
    public void TestCDCommandWorking() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test/");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("CWD /tmp/test/1\r\n");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;
        assertEquals(this.server.currentRepertory,"/tmp/test/1");
        assertEquals("200 Command OK",res);
    }
    @Test
    public void TestCDCommandNotWorking() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test/");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("CWD /tmp/56/1\r\n");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;
        assertEquals(this.server.currentRepertory,"/tmp/test/");
        assertEquals("550 Failed to change directory.",res);
    }

    @Test
    public void TestUnknowCommand() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test/");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("CD /tmp/56/1\r\n");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;
        assertEquals("202 Command not implemented, superfluous at this site.",res);
    }

    @Test
    public void TestCDUPCommandWorking() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("CWD /tmp/test/1\r\n");
        clientReceptor.readLine();

        clientReceptor.readLine();

        clientSent.println("CDUP \r\n");

        clientReceptor.readLine();
        String res = this.clientReceptor.readLine();


        assertEquals("/tmp/test", this.server.currentRepertory);
        assertEquals("200 Command OK",res);
        thd.runnable=false;
    }

    @Test
    public void TestCDUPCommandNotWorking() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("CDUP \r\n");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;
        assertEquals("/tmp/test", this.server.currentRepertory);
        assertEquals("550 Failed to change directory.",res);

    }

    @Test
    public void TestPWDCommand() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("PWD \r\n");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;

        assertEquals("200 \"/tmp/test\" is the current directory.",res);
    }

    @Test
    public void TestTypeCommand() throws IOException {
        thd.setServer(this.server);
        server.addUser("admin","admin");
        server.initRepository("/tmp/test");
        thd.start();
        clientReceptor.readLine();
        clientSent.println("USER admin");
        clientReceptor.readLine();
        clientSent.println("PASS admin");
        clientReceptor.readLine();
        clientSent.println("TYPE toto");
        String res = this.clientReceptor.readLine();
        thd.runnable=false;

        assertEquals("200 OK",res);
        assertEquals("toto", thd.type);
    }

    @After
    public void closeSocket() throws IOException {
        server.ss.close();
        client.close();
        thd=null;
    }

}
