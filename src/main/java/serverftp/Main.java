package serverftp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Nathan Houzet
 *
 */
public class Main {
    /***
     * init the server and wait for clients connextion
     * @param args 2 arguments , the port to launch the ftp server and the repository to init it
     */
    public static void main(String[] args){
        try {

            String repertory = args[1];
            int port = Integer.parseInt(args[0]);
            Server server = new Server(port);
            server.initRepository(repertory);
            server.addUser("admin","admin");
            ServerSocket serv_sock = server.getServerSocket();
            ArrayList<ConnectThread> clients = new ArrayList<>();
            System.out.println("Init server finish");

            int i = 0;
            while(true) {
                Socket new_connection = serv_sock.accept();
                ConnectThread new_client = new ConnectThread(new_connection);
                if (! clients.contains(new_client)){
                    clients.add(new_client);
                    new_client.setServer(server);
                    new_client.start();
                }

            }
        }catch(Exception e){System.out.println("Error: " + e.getMessage());}

    }
}
