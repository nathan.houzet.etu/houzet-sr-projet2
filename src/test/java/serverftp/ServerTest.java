package serverftp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test for the Server class.
 */
public class ServerTest
{
    Server server;
    @Before
    public void initServer() throws ErrorOfInitServerSocketException {
        this.server =  new Server((int) (8000 + (Math.random() * (9000 - 8000))));
    }

    @Test
    public void testUserHasmap(){
        server.addUser("nathan","hello");
        assertTrue(server.users.containsKey("nathan"));
        assertTrue(server.users.get("nathan").equals("hello"));

    }

    @Test
    public void testInitRepository() {
        server.initRepository("/tmp/toto");
        assertTrue(server.root.equals("/tmp/toto"));
        assertTrue(server.currentRepertory.equals("/tmp/toto"));

    }

    @Test
    public void TestGetServerSocket(){
        assertEquals(server.ss,server.getServerSocket());
    }

    @After
    public void closeSocket() throws IOException {
        server.ss.close();
    }

}
