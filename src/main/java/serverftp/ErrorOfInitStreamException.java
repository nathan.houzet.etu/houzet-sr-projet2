package serverftp;

/***
 * @author Nathan Houzet
 * Exception for error of output or input stream initiation
 */
public class ErrorOfInitStreamException extends Exception {
    /***
     * personalised exception
     * @param errorMessage the error message to be display if the exception is raise
     */
    public ErrorOfInitStreamException(String errorMessage) {
        super(errorMessage);
    }
}