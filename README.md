Server FTP  
Nathan Houzet  
2021

# Introduction 

L'objectif du projet est désormais de mettre en œuvre un serveur conforme au protocole applicatif File Transfer Protocol (FTP). Ce serveur doit donc utiliser l'API Socket TCP pour échanger avec un client FTP (e.g., Filezilla) pour stocker et envoyer des fichiers en respectant le standard FTP.

# Lancer le programme

Une [démo du programme](./doc/demoServerFtp.mp4) est disponible dans le dossier doc.

Pour lancer le serveur il faut : 
 - se placer à la racine du projet dans le dossier "projet2-serverftp"
 - executer la commande `mvn package` ( si tout est bien configuré, un dossier target avec un executable "tree-ftp-1.jar" sont générés )
 - executer le jar avec la commande `java -cp target/serverftp-1.jar serverftp.Main <port> <file>` avec <port> le numéro de port et <file> le fichier hôte du serveur.

Pour la configuration filezilla : 
 - Host : 127.0.0.1
 - Username : admin
 - Password : admin
 - Port : port choisi lors du lancement du serveur.

NB : l'utilisateur admin est implémenté par défaut.

Les parties initialisation du serveur FTP, authentification et navigation dans le serveur ont été traités.

# Architechture

Le programme est principalement composé de 3 classes.



**Une classe Server** : 

Cette classe gère le lancement du server ftp. Elle initialise principalement la ServerSocket et contient également les paramètres du serveur.

**Une classe ConnectThread** : 

Cette classe gère le comportement du client qui se connecte au serveur ftp.  Une 
première partie  du travail de cette classe concerne l'authentification du client. 
Ensuite la classe réagit face au différentes commmandes recus par le client. Elle gère 
également l'ouverture d'une socket de donné pour la mise en place du mode passif et 
les échanges de données.

Diagrammes UML des classes Connexion et FtpCommands : 

![Classes crées.](./doc/diagramUML.png)

**La classe Main** : 

Cette classe permet de récuperer les arguments et lancer les méthodes permettant de 
lancer le server via les paramètres reçus. Une fois le serveur lancé, le server va 
lancer un thread pour chaque nouvelle connexion.

**Les exceptions** : 

De plus, quatres exceptions ont été créés pour préciser les différentes pannes qu'il peut y avoir lors de l'éxecution du programme : 

- Une exception qui concerne la server socket, ci celle-ci n'arrive pas à se crée via le port voulu, alors l'exception "ErrorOfInitServerSocketException" est levé.

- Une exception qui concerne les flux pour communiquer avec le serveur. Si une erreur apparait lors de la création du flux d'entrée ou de sortie du serveur FTP alors l'exception "ErrorOfInitStreamException" est levé.

- Une exception concerne la lecture du flux de réponse du serveur. Comme pour chaque commande envoyé au serveur génére une réponse, il est alors intéressant de créer une exception si jamais une erreur se produit lors de la lecture de la réponse. Il d'agit de l'exeption "ErrorOfSystemReadingException".

- La dernière, concerne l'utilisation du bash dans le code. Si une commande bash ne fonctionne pas correctement, alors l'exception "ErrorWhilebashExecutingException" est déclenché.
 

# Parcours de code

**Code 1**
```java
private void SetPassiveMode() throws ErrorOfInitServerSocketException {
        this.passivePort = (int) (100 + (Math.random() * (200 - 100)));
        sendCommand.println("200 Entering Passive Mode (127,0,0,1,"+10+","+passivePort+")\r");
        try{
            this.pss = new ServerSocket((10*256)+passivePort);
            this.passiveSocket=pss.accept();
        } catch (IOException e) {
            sendCommand.println("425 Can’t open data connection.\r");
            throw new ErrorOfInitServerSocketException("Erreur lors de la création du server socket de data lors de la mise en mode passive");
        }
    }
```

Cette méthode implémente la mise du serveur en mode passive. On va ainsi retrouver les différentes étapes qui le 
permettent, tel que la création d'un serveur socket de donnée sur un port aléatoire. Ensuite on va transmettre les 
informations qui permettent au client de s'y connecter. Et on va attendre la connextion du client pour ensuite lui envoyer les données.


**Code 2**
```java
 try{
        p=Runtime.getRuntime().exec("ls -al /tmp/server");
        BufferedReader br=new BufferedReader(
                new InputStreamReader(p.getInputStream()));
        while((s=br.readLine())!=null){
            sendResponse(dataSend,s);
        }
        p.waitFor();
        p.destroy();
        }
```

C'est grâce à cette commande Bash executé au sein du code Java que l'on va lister tout les fichiers du répertoire 
pour ensuite l'envoyer au client. Mélanger le Bash et le Java permet de garder la facilité de conception et de programmation du 
Java tout en disposant de la puissance du Bash pour lister le contenu d'un dossier.




# Tests

Le projet à quasiment été testé entierement. Seule la méthode utilisé pour lister les 
répertoires n'a pas été testé.

Les 20 tests passent "au vert " lors de la commande `mvn package`. Si un problème 
intervient, c'est à cause de  la créations des servers sockets dans les classes de 
test. En effet les servers sockets se créent sur un port alétoire entre 8000 et 9000, 
il se peut ainsi que le port soit déjà utilisé. il faut alors réitérer la commande `mvn 
package`' 

Un mock d'un client a été crée pour tester les interactions client-serveur. 

![Tests crées.](./doc/Tests.png)

# Documentation

La documentation est disponible dans le dossier doc via le fichier [*index.html*](./doc/index.html) situé dans le fichier doc. 