package serverftp;

/***
 * @author Nathan Houzet
 * Exception for errors when reading a stream
 */
public class ErrorOfSystemReadingException extends Exception {
    /***
     * personalised exception
     * @param errorMessage the error message to be display if the exception is raise
     */
    public ErrorOfSystemReadingException(String errorMessage) {
        super(errorMessage);
    }
}